<?php

namespace App\Http\Controllers\Superadmin\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class RoleController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_group';
    }

    public function result_all(Request $request) {
        if ($request->type == 'table') {
            $role = $this->master->result_all(
                $this->table, 
                ['ID', 'GROUP_NAME', 'STATUS']
            );

            return $this->response_data(
                'Result Data Role Account Success!', 
                ['self' => url($request->fullURL())], 
                $role
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_list(Request $request) {
        if ($request->status != '') {
            $role = $this->master->results_filtering(
                $this->table, 
                ['STATUS' => $request->status], 
                ['ID', 'GROUP_NAME'],
                'GROUP_NAME',
                'asc'
            );

            return $this->response_data(
                'Result Data Role Account Success!', 
                ['self' => url($request->fullURL())], 
                $role
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $role = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            ['ID', 'GROUP_NAME', 'STATUS']
        );

        if ($role) {
            return $this->response_data(
                'Result Data Role Account Success!', 
                ['self' => url(Request()->fullURL())], 
                $role
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        if ($request->role == '' && $request->status == '') {
            return $this->response_message(
                "Role & Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->role == '') {
            return $this->response_message(
                "Role tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->status == '') {
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data Role
                $update_role = $this->master->updates(
                    $this->table, 
                    ['ID' => $request->id], ['GROUP_NAME' => $request->role, 'STATUS' => $request->status]
                );
    
                if ($update_role == true) {
                    return $this->response_message(
                        'Data Role Success Updated!', 
                        ['self' => url($request->fullURL())],
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Role Failed Updated!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                // Create Data Role
                $insert_role = $this->master->create(
                    $this->table, 
                    ['GROUP_NAME' => $request->role, 'STATUS' => $request->status]
                );
    
                if ($insert_role == true) {
                    return $this->response_message(
                        'Data Role Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Role Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }
}
