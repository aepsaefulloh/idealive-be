<?php

namespace App\Http\Controllers\Superadmin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class StockController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_stock';
        $this->table_product = 'tbl_product';
        $this->destination_product = 'storage/images/products/';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $stock = $this->master->results_filtering(
                $this->table,
                null, 
                ['ID', 'STOCK_DATE', 'CODE', 'SIZE', 'TOTAL'],
                'STOCK_DATE',
                'desc'
            );    

            if (count($stock) > 0) {
                foreach ($stock as $key => $s) {
                    // Find Product By id
                    $find_product = $this->master->result_filtering($this->table_product, ['CODE' => $s->CODE], ['PRODUCT']);

                    $results[$key] = [
                        "ID"                => $s->ID,
                        "STOCK_DATE"        => $s->STOCK_DATE,
                        "CODE"              => $s->CODE,
                        "PRODUCT"           => (isset($find_product)) ? $find_product->PRODUCT : null,
                        "SIZE"              => $s->SIZE,
                        "TOTAL"             => $s->TOTAL
                    ];
                }
            } else {
                $results = [];
            }
            

            return $this->response_data(
                'Result Data Stock Product Success!', 
                ['self' => url($request->fullURL())], 
                $results
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        // Find Stock By ID
        $stock = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            ['ID', 'STOCK_DATE', 'CODE', 'SIZE', 'WIDTH', 'LENGTH', 'TOTAL', 'STATUS']
        );

        if ($stock) {   
            return $this->response_data(
                'Result Data Product Stock Success!', 
                ['self' => url(Request()->fullURL())], 
                $stock
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function stock_balance(Request $request) {
        // if ($request->page != '') {
            $product = $this->master->results_filtering(
                $this->table_product,
                null, 
                ['ID', 'CODE', 'PRODUCT', 'IMAGE'],
                'ID',
                'desc'
            );
            
            // // Paginate Data
            // $product = $products->forPage($request->page, $request->per_page);

            if (count($product) > 0) {
                foreach ($product as $key => $p) {
                    // Find Stock Size Product S
                    $stock_s = $this->master->result_filtering(
                        $this->table,
                        [['CODE', $p->CODE], ['SIZE', 1]], 
                        ['ID', 'TOTAL'],
                    );

                    // Find Stock Size Product M
                    $stock_m = $this->master->result_filtering(
                        $this->table,
                        [['CODE', $p->CODE], ['SIZE', 2]], 
                        ['ID', 'TOTAL'],
                    );

                    // Find Stock Size Product L
                    $stock_l = $this->master->result_filtering(
                        $this->table,
                        [['CODE', $p->CODE], ['SIZE', 3]], 
                        ['ID', 'TOTAL'],
                    );

                    // Find Stock Size Product XL
                    $stock_xl = $this->master->result_filtering(
                        $this->table,
                        [['CODE', $p->CODE], ['SIZE', 4]], 
                        ['ID', 'TOTAL'],
                    );

                    $results[$key] = [
                        "ID"                => $p->ID,
                        "CODE"              => $p->CODE,
                        "PRODUCT"           => $p->PRODUCT,
                        "IMAGE"             => ($p->IMAGE != '') ? url($this->destination_product.$p->IMAGE) : null,
                        "STOCK"             => [
                            "S"     => (isset($stock_s)) ? $stock_s->TOTAL : 0,
                            "M"     => (isset($stock_m)) ? $stock_m->TOTAL : 0,
                            "L"     => (isset($stock_l)) ? $stock_l->TOTAL : 0,
                            "XL"    => (isset($stock_xl)) ? $stock_xl->TOTAL : 0
                        ]
                    ];
                }
            // } else {
            //     $results = [];
            // }
            

            return $this->response_data(
                'Result Data Stock Balance Product Success!', 
                [
                    'self'      => url($request->fullURL()),
                    // 'page'      => $request->page,
                    // 'per_page'  => $request->per_page,
                    // 'all_page'  => (count($products) > 0 && $request->page != '') ? ceil(count($products) / $request->per_page) : 0,
                    // 'all_data'  => count($products)
                ], 
                $results
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        // Validasi Request
        if ($request->date == '' && $request->product == '' && $request->size == '' && $request->total == '' && $request->width == '' && $request->length == '' && $request->status == '') {
            return $this->response_message(
                "Date, Product, Size, Width, Length, Total & Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->date == '') { 
            return $this->response_message(
                "Date tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->product == '') { 
            return $this->response_message(
                "Product tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->size == '') { 
            return $this->response_message(
                "Size tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->total == '') { 
            return $this->response_message(
                "Total tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->width == '') { 
            return $this->response_message(
                "Width tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->length == '') { 
            return $this->response_message(
                "Length tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->status == '') { 
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data Stock
                $update_stock = $this->master->updates($this->table,
                    ["ID" => $request->id],
                    [
                        "STOCK_DATE"    => $request->date,
                        "CODE"          => $request->product,
                        "SIZE"          => $request->size,
                        "WIDTH"         => $request->width,
                        "LENGTH"        => $request->length,
                        "TOTAL"         => $request->total,
                        "STATUS"        => $request->status,
                    ]
                );

                if ($update_stock == true) {
                    return $this->response_message(
                        'Data Stock Success Updated!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Stock Failed Updated!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                // Create Data Stock
                $insert_stock = $this->master->create($this->table, [
                    "STOCK_DATE"    => $request->date,
                    "CODE"          => $request->product,
                    "SIZE"          => $request->size,
                    "WIDTH"         => $request->width,
                    "LENGTH"        => $request->length,
                    "TOTAL"         => $request->total,
                    "STATUS"        => $request->status,
                ]);
    
                if ($insert_stock == true) {
                    return $this->response_message(
                        'Data Stock Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Stock Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }
}
