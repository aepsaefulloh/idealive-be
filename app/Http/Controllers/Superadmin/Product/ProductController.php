<?php

namespace App\Http\Controllers\Superadmin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_product';
        $this->table_add_image = 'tbl_addimage';
        $this->table_category = 'tbl_category';
        $this->destination = 'storage/images/products/';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $product = $this->master->results_filtering(
                $this->table,
                [['STATUS', '<>', 99]], 
                ['ID', 'CODE', 'PRODUCT', 'CATEGORY', 'PRICE', \DB::raw("CONCAT('".url($this->destination)."', '/', IMAGE) AS IMAGE"), 'STATUS', 'AVAIL'],
                'ID',
                'desc'
            );    

            if (count($product) > 0) {
                foreach ($product as $key => $p) {
                    // Find Category By id
                    $find_category = $this->master->result_filtering($this->table_category, ['ID' => $p->CATEGORY], ['CATEGORY']);

                    $results[$key] = [
                        "ID"        => $p->ID,
                        "CODE"      => $p->CODE,
                        "PRODUCT"   => $p->PRODUCT,
                        "CATEGORY"  => $find_category->CATEGORY,
                        "PRICE"     => $p->PRICE,
                        "IMAGE"     => $p->IMAGE,
                        "STATUS"    => $p->STATUS,
                        "AVAIL"     => $p->AVAIL
                    ];
                }
            } else {
                $results = [];
            }
            

            return $this->response_data(
                'Result Data Product Success!', 
                ['self' => url($request->fullURL())], 
                $results
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function results_list() {
        $product= $this->master->results_filtering(
            $this->table,
            [['STATUS', 1]], 
            ['CODE','PRODUCT'],
            'PRODUCT',
            'asc'
        );    

        return $this->response_data(
            'Result List Data Product Success!', 
            ['self' => url(Request()->fullURL())], 
            $product
        );
    }

    public function result_find($id) {
        // Find Product By ID
        $product = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            ['ID', 'CODE', 'PRODUCT', 'CATEGORY', 'PRICE', \DB::raw("CONCAT('".url($this->destination)."', '/', IMAGE) AS IMAGE"), 'SUMMARY', 'SPECS', 'STATUS', 'AVAIL', 'CREATE_DATE'],
        );

        if ($product) {   
            // Find Add Image 
            $add_image = $this->master->results_filtering(
                $this->table_add_image,
                [['PRODUCT_ID', $id], ['STATUS', 1]], 
                ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE")],
                'ID',
                'asc'
            );

            // Result Product Final
            $result = [
                "ID"            => $product->ID,
                "CODE"          => $product->CODE,
                "PRODUCT"       => $product->PRODUCT,
                "CATEGORY"      => $product->CATEGORY,
                "PRICE"         => $product->PRICE,
                "IMAGE"         => [
                    "DEFAULT"   => $product->IMAGE,
                    "ADD_IMAGE" => $add_image
                ],
                "SUMMARY"       => $product->SUMMARY,
                "SPECS"         => $product->SPECS,
                "STATUS"        => $product->STATUS,
                "AVAIL"         => $product->AVAIL,
                "CREATE_DATE"   => date('Y-m-d\TH:i', strtotime($product->CREATE_DATE))
            ];

            return $this->response_data(
                'Result Data Product Account Success!', 
                ['self' => url(Request()->fullURL())], 
                $result
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        // Validasi Request
        if ($request->product == '' && $request->code == '' && $request->category == '' && $request->status == '' && $request->price == '' && $request->specs == '' && $request->date == '') {
            return $this->response_message(
                "Nama Product, Code, Price, Category, Specification, Tanggal & Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->product == '') { 
            return $this->response_message(
                "Nama Product tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->code == '') { 
            return $this->response_message(
                "Code Product tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->category == '') { 
            return $this->response_message(
                "Category tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->price == '') { 
            return $this->response_message(
                "Price tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->specs == '') { 
            return $this->response_message(
                "Specification tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->date == '') { 
            return $this->response_message(
                "Tanggal tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->status == '') { 
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // find data with id request
                $product = $this->master->result_filtering($this->table, ['ID' => $request->id], ['IMAGE']);

                if ($product) {
                    // Save Add Image Product
                    if (isset($request->image) && $request->image != '') {
                        // Looping Image Code base64 & Save Directory
                        foreach ($request->image as $key => $image) {     
                            // Save Image From Directory
                            $imageName[$key] = uniqid().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $this->ext_base64($image);
                            $this->save_image($image, $imageName[$key], $this->destination, $this->ext_base64($image)); 
                        }

                        // Insert Data Add Image to DB
                        foreach ($imageName as $key => $in) {
                            $insert_addimage[$key] = $this->master->create($this->table_add_image, [
                                "PRODUCT_ID"    => $request->id,
                                "IMAGE"         => $in,
                                "STATUS"        => 1
                            ]);   
                        }
                    }

                    // Save Image Default Product
                    if (isset($request->image_def) && $request->image_def != '') {
                        // Save Image From Directory
                        $imageNameDef = uniqid().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $this->ext_base64($request->image_def);
                        $this->save_image($request->image_def, $imageNameDef, $this->destination, $this->ext_base64($request->image_def));
                        
                        // Update Data Product to database
                        $update_product = $this->master->updates($this->table,
                            ['ID' => $request->id], 
                            [
                                "CODE"          => $request->code,
                                "PRODUCT"       => $request->product,
                                "CATEGORY"      => $request->category,
                                "SUMMARY"       => $request->summary,
                                'IMAGE'         => $imageNameDef,
                                "PRICE"         => $request->price,
                                "SPECS"         => $request->specs,
                                "STATUS"        => $request->status,
                                "AVAIL"         => $request->avail,
                                "CREATE_DATE"   => $request->date,
                            ]
                        );
                    } else {
                        // Update Data Product to database
                        $update_product = $this->master->updates($this->table,
                            ['ID' => $request->id], 
                            [
                                "CODE"          => $request->code,
                                "PRODUCT"       => $request->product,
                                "CATEGORY"      => $request->category,
                                "SUMMARY"       => $request->summary,
                                "PRICE"         => $request->price,
                                "SPECS"         => $request->specs,
                                "STATUS"        => $request->status,
                                "AVAIL"         => $request->avail,
                                "CREATE_DATE"   => $request->date,
                            ]
                        );
                    }
                    
                    if ($update_product == true || (isset($insert_addimage) && $insert_addimage == true)) {
                        return $this->response_message(
                            'Data Product Success Updated!', 
                            ['self' => url($request->fullURL())], 
                            200
                        );
                    } else {
                        return $this->response_message(
                            'Data Product Failed Updated!', 
                            ['self' => url($request->fullURL())], 
                            404
                        );
                    }
                } else {
                    return $this->response_message(
                        'Data Product Not Found!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                // Cek JIka Image ada
                if ($request->image != '' && $request->image_def != '') {
                    // Looping Image Code base64
                    foreach ($request->image as $key => $image) {
                        // Save Image From Directory
                        $imageName[$key] = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $this->ext_base64($image);
                        $this->save_image($image, $imageName[$key], $this->destination, $this->ext_base64($image)); 
                    }

                    // Save Image Default Product
                    $imageNameDef = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $this->ext_base64($request->image_def);
                    $this->save_image($request->image_def, $imageNameDef, $this->destination, $this->ext_base64($request->image_def));

                    // Insert Data Product to database
                    $insert_product = $this->master->getID_create($this->table, 
                        [
                            "CODE"          => $request->code,
                            "PRODUCT"       => $request->product,
                            "CATEGORY"      => $request->category,
                            "SUMMARY"       => $request->summary,
                            "PRICE"         => $request->price,
                            "SPECS"         => $request->specs,
                            "IMAGE"         => $imageNameDef,
                            "STATUS"        => $request->status,
                            "AVAIL"         => $request->avail,
                            "CREATE_DATE"   => $request->date,
                        ]
                    );

                    // Insert Data Image To database
                    foreach ($imageName as $key => $in) {
                        $insert_addimage[$key] = $this->master->create($this->table_add_image, [
                            "PRODUCT_ID"    => $insert_product,
                            "IMAGE"         => $in,
                            "STATUS"        => 1
                        ]);   
                    }
                } 

                if ($insert_addimage == true) {
                    return $this->response_message(
                        'Data Product Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Product Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }

    public function remove_image(Request $request) {
        if ($request->type == 'default' && $request->id != '') {
            // Find Data Image On Product
            $image_product = $this->master->updates($this->table,
                ['ID' => $request->id],
                ['IMAGE' => null]
            );

            if ($image_product == true) {
                return $this->response_message(
                    'Image Success Deleted!', 
                    ['self' => url($request->fullURL())], 
                    200
                );       
            } else {
                return $this->response_message(
                    'Image Failed Deleted!', 
                    ['self' => url($request->fullURL())], 
                    400
                );
            }
        } elseif ($request->type == 'add_image' && $request->id != '') {
            // Find Data Image On Product
            $image_product = $this->master->updates($this->table_add_image,
                ['IMAGE' => $request->id],
                ['STATUS' => 99]
            );

            if ($image_product == true) {
                return $this->response_message(
                    'Image Success Deleted!', 
                    ['self' => url($request->fullURL())], 
                    200
                );       
            } else {
                return $this->response_message(
                    'Image Failed Deleted!', 
                    ['self' => url($request->fullURL())], 
                    400
                );
            }
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function status_product(Request $request) {
        // Validasi Request
        if ($request->id == '' || $request->status == '') {
            return $this->response_message(
                "Failed!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            // find data with id request
            $product = $this->master->result_filtering($this->table, ['ID' => $request->id], ['IMAGE']);

            if ($product) {
                // Update Data Product to database
                $update_product = $this->master->updates($this->table,
                    ['ID' => $request->id], 
                    [
                        "STATUS"        => $request->status,
                    ]
                );

                if ($update_product == true) {
                    return $this->response_message(
                        'Data Product Success Updated Status!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Product Failed Updated Status!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                return $this->response_message(
                    'Data Product Not Found!', 
                    ['self' => url($request->fullURL())], 
                    404
                );
            }
        }
    }

}
