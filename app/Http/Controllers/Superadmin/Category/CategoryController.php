<?php

namespace App\Http\Controllers\Superadmin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class CategoryController extends Controller
{
    public function __construct() {
        // $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_category';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $category = $this->master->results_filtering(
                $this->table,
                [['STATUS', '<>', 99]], 
                ['ID', 'CATEGORY', 'SEO', 'TIPE', 'STATUS'],
                'CATEGORY',
                'asc'
            );    

            return $this->response_data(
                'Result Data Category Success!', 
                ['self' => url($request->fullURL())], 
                $category
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function results_list(Request $request) {
        if ($request->status != '' && $request->type != '') {
            $category = $this->master->results_filtering(
                $this->table,
                [['STATUS', $request->status], ['TIPE', $request->type]], 
                ['ID', 'CATEGORY'],
                'CATEGORY',
                'asc'
            );    

            return $this->response_data(
                'Result List Data Category Success!', 
                ['self' => url($request->fullURL())], 
                $category
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $category = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            ['ID', 'LEVEL', 'PARENT_ID', 'CATEGORY', 'SEO', 'TIPE', 'STATUS']
        );

        if ($category) {
            return $this->response_data(
                'Result Data Category Success!', 
                ['self' => url(Request()->fullURL())], 
                $category
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        // Validasi Request
        if ($request->category == '' && $request->seo == '' && $request->tipe == '' && $request->status == '') {
            return $this->response_message(
                "Category, SEO, Tipe & Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->category == '') { 
            return $this->response_message(
                "Category tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->seo == '') { 
            return $this->response_message(
                "SEO tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->tipe == '') { 
            return $this->response_message(
                "Tipe tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->status == '') { 
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data Banner
                $update_category = $this->master->updates($this->table,
                    ["ID" => $request->id],
                    [
                        "CATEGORY"      => $request->category,
                        "SEO"           => $request->seo,
                        "TIPE"          => $request->tipe,
                        "STATUS"        => $request->status,
                        "LEVEL"         => $request->level,
                        "PARENT_ID"     => $request->parent_id
                    ]
                );

                if ($update_category == true) {
                    return $this->response_message(
                        'Data Category Success Updated!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Category Failed Updated!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                // Create Data Category
                $insert_category = $this->master->create($this->table, [
                    "CATEGORY"      => $request->category,
                    "SEO"           => $request->seo,
                    "TIPE"          => $request->tipe,
                    "STATUS"        => $request->status,
                    "LEVEL"         => $request->level,
                    "PARENT_ID"     => $request->parent_id
                ]);
    
                if ($insert_category == true) {
                    return $this->response_message(
                        'Data Category Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Category Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }
}
