<?php

namespace App\Http\Controllers\Superadmin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;
use Illuminate\Support\Facades\File;

class BannerController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_banners';
        $this->destination = 'storage/images/banners/';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $banner = $this->master->results_filtering(
                $this->table,
                [['STATUS', '<>', 99]], 
                ['ID', 'TITLE', 'TYPE', 'POS', \DB::raw("CONCAT('". url($this->destination) ."', '/', FILENAME) AS FILENAME"), 'URL', 'ORDERNUM', 'STATUS'],
                'CREATE_DATE',
                'desc'
            );    

            return $this->response_data(
                'Result Data Banner Success!', 
                ['self' => url($request->fullURL())], 
                $banner
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $banner = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            ['ID', 'TITLE', 'TYPE', 'POS', \DB::raw("CONCAT('". url($this->destination) ."', '/', FILENAME) AS FILENAME"), 'URL', 'ORDERNUM', 'STATUS'],
        );

        if ($banner) {
            return $this->response_data(
                'Result Data Role Account Success!', 
                ['self' => url(Request()->fullURL())], 
                $banner
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        // Validasi Request
        if ($request->title == '' && count($request->image) == 0 && $request->position == '' && $request->status == '' && $request->ordernum == '') {
            return $this->response_message(
                "Judul, Posisi, Urutan Ke, Gambar & Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->title == '') { 
            return $this->response_message(
                "Judul tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif (count($request->image) == 0) { 
            return $this->response_message(
                "Gambar tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->position == '') { 
            return $this->response_message(
                "Posisi tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->status == '') { 
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->ordernum == '') { 
            return $this->response_message(
                "Urutan Banner tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data Banner
                $search_image_old = $this->master->result_filtering($this->table, ['ID' => $request->id], ['FILENAME']);

                if ($search_image_old) {
                    if ($request->image['name'] != '') {
                        // Save Image
                        $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->image['type'];
                        $this->save_image($request->image['name'], $imageName, $this->destination, $request->image['type']);
                        
                        // Delete Image
                        $this->delete_file($this->destination.$search_image_old->FILENAME);
                    } else {
                        $imageName = $search_image_old->FILENAME;
                    }
    
                    $update_banner = $this->master->updates($this->table,
                        ["ID" => $request->id],
                        [
                            "TITLE"         => $request->title,
                            "TYPE"          => $request->type,
                            "POS"           => $request->position,
                            "FILENAME"      => $imageName,
                            "URL"           => $request->url,
                            "STATUS"        => $request->status,
                            "ORDERNUM"      => $request->ordernum,
                            "UPDATE_DATE"   => $this->now_date('Y-m-d G:i:s'),
                        ]
                    );

                    if ($update_banner == true) {
                        return $this->response_message(
                            'Data Banner Success Updated!', 
                            ['self' => url($request->fullURL())], 
                            200
                        );
                    } else {
                        return $this->response_message(
                            'Data Banner Failed Updated!', 
                            ['self' => url($request->fullURL())], 
                            404
                        );
                    }
                } else {
                    return $this->response_message(
                        'Data Banner Not Found!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                if ($request->image['name'] != '') {
                    $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->image['type'];
                    $this->save_image($request->image['name'], $imageName, $this->destination, $request->image['type']);
                } else {
                    $imageName = 'default.png';
                }

                // Create Data Banner
                $insert_banner = $this->master->create($this->table, [
                    "TITLE"         => $request->title,
                    "TYPE"          => $request->type,
                    "POS"           => $request->position,
                    "FILENAME"      => $imageName,
                    "URL"           => $request->url,
                    "STATUS"        => $request->status,
                    "ORDERNUM"      => $request->ordernum,
                    "CREATE_DATE"   => $this->now_date('Y-m-d G:i:s')
                ]);
    
                if ($insert_banner == true) {
                    return $this->response_message(
                        'Data Banner Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Banner Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }
}
