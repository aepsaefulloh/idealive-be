<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Master;
use App\Models\PHPMail;

class ToolsController extends Controller
{
    public function __construct() {
        $this->middleware('api_client');
        $this->table_prov = 'tbl_prov';
        $this->master = New Master;
        $this->mailer = New PHPMail;
    }

    // ========================================================= Location ================================================================ //
    public function get_province(Request $request) {
        if ($request->prov && $request->prov != '') {
            $prov = $this->master->result_filtering(
                $this->table_prov,
                ['ID' => $request->prov],
                ['ID', 'PROV']
            );
        } else {
            $prov = $this->master->results_filtering(
                $this->table_prov,
                ['STATUS' => 1],
                ['ID', 'PROV'],
                "PROV",
                "asc"
            );
        }
        
        return $this->response_data(
            'Result Data Province Success!', 
            [
                'self'          => url($request->fullURL()),
            ], 
            $prov
        );
    }

    public function send_email() {
        // End Email Notifikasi
        $text='<h3 class="mb-3">Thank you for shopping with STUDIO POP</h3>';
        $text.="<p><strong>Hi,&nbsp;Doni S</strong></p>";
        $text.="<p class='mb-3'><strong>Thank you for your order! Your items will be delivered after the payment has been made. Please make your payment within 24 hours to avoid order cancellation and send the payment receipt to shopping@studiopop.id</strong></p>";
        $text.="<div class='box-text'>";
        $text.="<p><strong>Order No : ST8997897897987987987</strong></p>";
        $text.="<p><strong>SubTotal : Rp. ".number_format(1902923)."</strong></p>";
        $text.="<p><strong>Shipping : Rp. ".number_format(323823)."</strong></p>";
        $text.="<p class='mb-3'><strong>Total &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Rp. ".number_format(382398232)."</strong></p>";
        $text.="<p><i><strong>Please settle the payment by transferring it to the following bank account with your order number as the reference.<br><br>

        BCA 7310160371 a/n Daiva Prayudi </strong></i></p>";
        $text.='<hr class="dash"></br>';

        $text.='<center><p class="text-center">STUDIO POP Terms</p>';
        $text.='<p class="text-center">All sales are final. No refunds. No exchanges.</p><br>';

        $text.='<p class="text-center">Shipping</p>';
        $text.='<p class="text-center">Kindly make sure that the shipping address is correct and somebody is home to receive the package from 9 am to 6 pm on the estimated delivery date.</p><br>';

        $text.='<p class="text-center">Please transfer to</p>';

        $text.='<p class="text-center">BCA 7310160371 a/n Daiva Prayudi</p><br>';

        $text.='<p class="text-center">Please complete your payment within 24 hours. The order will automatically be canceled if there’s no payment within 24 hours.</p>';
        $text.='<p class="text-center">Please send a confirmation after you make the payment along with the prove of payment</p><br>';

        $text.='<p class="text-center">Once again, thank you for shopping with STUDIO POP!</p><br>';

        $text.='<p class="text-center"><strong>STUDIO POP</strong></p>';
        $text.='<p class="text-center">www.studiopop.id</p></div></center>';

        $msc['NAME']='Doni S';
        $msc['MAILTO']='doni.sdr1001@gmail.com';
        $msc['SUBJECT']='Studio Pop Notificaton';
        $msc['BODY']=$text;
        $send = $this->mailer->sendMail($msc);

        return response()->json($send);
    }
}
