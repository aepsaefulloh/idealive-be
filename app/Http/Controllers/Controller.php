<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;


class Controller extends BaseController
{
    // --------------------------- Response Return ---------------------------------------------------------- //
    public function response_data($message, $url, $data, $code=200) {
        return response()->json([
            "status"    => $code,
            "message"   => $message,
            "links"     => $url,
            "data"      => $data
        ]);
    }

    public function response_message($message, $url, $code=200) {
        return response()->json([
            "status"    => $code,
            "message"   => $message,
            "links"     => $url
        ]);
    }

    // ---------------------------------- Save Image ---------------------------------------------------------- //
    public function Save_image($imageFile, $imageName, $destination, $format_file) {
        // Decode Base64 Code Image
        $image = str_replace("[removed]", "", $imageFile);
        $img = preg_replace('/^data:image\/\w+;base64,/', '', $image);
        $decode_image = base64_decode($img);

        // Image Save
        if ($format_file == 'gif') {
            File::put($destination . $imageName, $decode_image);
        }
        else {
            $make_image = Image::make($decode_image)->save($destination . $imageName);
        }
    }

    // -------------------------------- Get Extentions File from base64 code -------------------------------- //
    public function ext_base64($code) {
        $img = explode(',', $code);
        $ini =substr($img[0], 11);
        $type = explode(';', $ini);
        return $type[0];  
    }

    // ------------------------------- Delete File ---------------------------------------------------------- //
    public function delete_file($destination) {
        // Delete File
        $delete = File::delete($destination);
    }

    // ------------------------------------- Date Time ------------------------------------------------------ //
    public function now_date($format) {
        return \Carbon\Carbon::now()->format($format);
    }
}
