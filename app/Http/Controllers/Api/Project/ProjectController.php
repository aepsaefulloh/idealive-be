<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class ProjectController extends Controller
{
    public function __construct() {
        $this->middleware('api_client');
        $this->table = 'tbl_project';
        $this->master = New Master;
        $this->destination = 'storage/images/projects/';
    }

    public function result_all_jurnal_playlist(Request $request) {
        $limit = ($request->limit != '') ? $request->limit : null;

        $project = $this->master->result_content_journal_playlist($this->table, ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'TITLE', 'SUBTITLE', 'SUMMARY', 'CATEGORY'], $limit);
        
        return $this->response_data(
            'Result Data Project Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "limit"     => $limit
                ],
            ], 
            $project
        );
    }

    public function result_all(Request $request) {
        $page = ($request->page != '') ? $request->page : 1;
        $perpage = ($request->perpage) ? $request->perpage : 10 ;
        $where = [
            ['STATUS', 1]
        ];
        if($request->category != '') {
            $where[] = ['CATEGORY', $request->category];
        }
        $project = $this->master->result_content(
            $this->table, 
            $where, 
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'TITLE', 'SUMMARY', 'SUBTITLE','CREATE_BY','CREATE_TIMESTAMP','CATEGORY', 'CAPTION'], 
             $page, 
             $perpage,
            'ID',
        );
        
        // Count Data All
        $count_data = $this->master->count_result_content(
            $this->table, 
            $where, 
        ); 
        
        return $this->response_data(
            'Result Data Project Journal Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "count_data"=> $count_data,
                    "page"      => $page,
                    "perpage"   => $perpage
                ],
            ], 
            $project
        );
    }

    public function result_find($id) {
        $project = $this->master->result_find_content(
            $this->table, 
            ['ID' => $id], 
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'TITLE', 'SUMMARY', 'CONTENT', 'CREATE_BY', 'CREATE_TIMESTAMP', 'SUBTITLE', 'CATEGORY','CAPTION']
        );
        
        return $this->response_data(
            'Result Data Project Success!', 
            [
                'self'          => url(Request()->fullURL()),
            ], 
            $project
        );
    }

    public function result_category(Request $request, $id) {
        $page = ($request->page != '') ? $request->page : 1;
        $perpage = ($request->perpage) ? $request->perpage : 10 ;
        $category = $request->category;
        $project = $this->master->result_content(
            $this->table, 
            [['STATUS', 1], ['CATEGORY', $id]], 
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'TITLE', 'SUMMARY', 'CONTENT', 'CREATE_BY', 'CREATE_TIMESTAMP', 'SUBTITLE', 'CATEGORY','CAPTION'], 
            $page,
            $perpage,
        );
        
        // Count Data All
        $count_data = $this->master->count_result_content(
            $this->table, 
            [['STATUS', 1]], 
        ); 
        
        return $this->response_data(
            'Result Data Project Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "count_data"=> $count_data,
                    "page"      => $page,
                    "perpage"   => $perpage
                ],
            ], 
            $project
        );
    }
}