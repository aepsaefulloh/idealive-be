<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class ProductController extends Controller
{
    public function __construct() {
        $this->middleware('api_client');
        $this->table = 'tbl_product';
        $this->master = New Master;
        $this->destination = 'storage/images/products/';
    }

    public function result_all(Request $request) {
        $page = ($request->page != '') ? $request->page : 1;
        $perpage = ($request->perpage) ? $request->perpage : 10 ;
        $product = $this->master->result_product(
            $this->table, 
            [['STATUS', 1]], 
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'PRODUCT', 'PRICE', 'AVAIL', 'SPECS','PRICE_BF','CATEGORY','CREATE_DATE'], 
            $page,
            $perpage,
        );
        
        // Count Data All
        $count_data = $this->master->count_result_product(
            $this->table, 
            [['STATUS', 1]], 
        ); 
        
        return $this->response_data(
            'Result Data Product Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "count_data"=> $count_data,
                    "page"      => $page,
                    "perpage"   => $perpage
                ],
            ], 
            $product
        );
    }

    public function result_category(Request $request, $id) {
        $page = ($request->page != '') ? $request->page : 1;
        $perpage = ($request->perpage) ? $request->perpage : 10 ;
        $category = $request->category;
        $product = $this->master->result_product(
            $this->table, 
            [['STATUS', 1], ['CATEGORY', $id]], 
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'PRODUCT', 'PRICE', 'AVAIL', 'SPECS','PRICE_BF','CATEGORY'], 
            $page,
            $perpage,
        );
        
        // Count Data All
        $count_data = $this->master->count_result_product(
            $this->table, 
            [['STATUS', 1]], 
        ); 
        
        return $this->response_data(
            'Result Data Product Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "count_data"=> $count_data,
                    "page"      => $page,
                    "perpage"   => $perpage
                ],
            ], 
            $product
        );
    }    

    public function result_find($id) {
        $product = $this->master->result_find_product(
            $this->table, 
            [['ID', $id], ['STATUS', 1]], 
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'CODE', 'PRODUCT', 'PRICE','PRICE_BF', 'SPECS', \DB::raw("CONCAT('0') AS SIZE"), 'AVAIL']
        );

        if ($product) {
            // Find Add Image Product
            $add_image = $this->master->result_add_image(
                'tbl_addimage',
                [['PRODUCT_ID', $product->ID], ['STATUS', 1]],
                ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE")]
            );  

            if ($add_image && count($add_image) > 0) {
                $image = [];
                $image[0] = [ "KEY" => 0, "NAME" => $product->IMAGE];
                foreach ($add_image as $key => $value) {
                    $image[$key+1] = [
                        "KEY"   => $key+1,
                        "NAME"  => $value->IMAGE  
                    ];
                }
                $product->IMAGE = $image;
            } else {
                $product->IMAGE = [["KEY" => 0, "NAME" => $product->IMAGE]];
            }

            // Find Size Product
            $size = $this->master->result_add_image(
                'tbl_stock',
                [['CODE', $product->CODE], ['TOTAL', '<>', 0]],
                ['ID', 'SIZE', 'WIDTH', 'LENGTH']
            );

            if ($size && count($size) > 0) {
                $product->SIZE = $size;
            } else {
                $product->SIZE = [];
            }
        } 
        
        
        return $this->response_data(
            'Result Data Product Success!', 
            [
                'self'  => url(Request()->fullURL()),
            ], 
            $product
        );
    }
}
