<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class CategoryController extends Controller
{
    public function __construct() {
        $this->middleware('api_client');
        $this->table = 'tbl_category';
        $this->master = New Master;
        $this->destination = 'storage/images/products/';
    }

    public function result_all(Request $request) {
        $page = ($request->page != '') ? $request->page : 1;
        $perpage = ($request->perpage) ? $request->perpage : 10 ;
        $product = $this->master->result_product(
            $this->table, 
            [['STATUS', 1]], 
            ['ID', 'CATEGORY'], 
            $page,
            $perpage,
        );
        
        // Count Data All
        $count_data = $this->master->count_result_product(
            $this->table, 
            [['STATUS', 1]], 
        ); 
        
        return $this->response_data(
            'Result Data Product Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "count_data"=> $count_data,
                    "page"      => $page,
                    "perpage"   => $perpage
                ],
            ], 
            $product
        );
    }

    public function result_find($name) {
        $product = $this->master->result_find_product(
            $this->table, 
            [['CATEGORY', str_replace('%20', ' ', $name)], ['STATUS', 1]], 
            ['ID', 'CATEGORY', 'TIPE', 'STATUS']
        );
        
        return $this->response_data(
            'Result Data Product Success!', 
            [
                'self'  => url(Request()->fullURL()),
            ], 
            $product
        );
    }
}
