<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;
use App\Models\PHPMail;

class PaymentController extends Controller
{
    public function __construct() {
        $this->middleware('api_client');
        $this->table = 'tbl_transaction';
        $this->table_2 = 'tbl_transaction_dtl';
        $this->master = New Master;
        $this->php_mail = New PHPMail;
        $this->destination = 'storage/images/products/';
    }

    public function checkout_store(Request $request) {
        // Insert Transaction
        $tr_id = 'ST'.time();
        $data['satu'] = [
            "TRDATE"    => \Carbon\Carbon::now()->format('Y-m-d G:i:s'),
            "TRID"      => $tr_id,
            "FULLNAME"  => $request->name,
            "ADDRESS"   => $request->address,
            "PHONE"     => $request->phone,
            "EMAIL"     => $request->email,
            "PROV"      => $request->province,
            "KAB"       => $request->city,
            "KODEPOS"   => $request->postal_code,
            "SHIPMENT"  => $request->courier,
            "SHIPPING"  => $request->shipping,
            "PAYMENT"   => $request->payment,
            "TOTAL"     => $request->count_payment,
            "STATUS"    => 0
        ];

        $insert_transaction = $this->master->getID_create($this->table, $data['satu']);

        // Insert Transaction Detail
        $data['dua'] = [];
        $product = [];
        foreach ($request->product as $key => $value) {
            $data['dua'][$key] = [
                "TRID"      => $insert_transaction,
                "CODE"      => $value['CODE'],
                "QTY"       => $value['QTY'],
                "SIZE"      => $value['SIZE'],
                "PRICE"     => $value['PRICE'],
                "TOTAL"     => $value['COUNT_PRICE'],
                "STATUS"    => 0
            ];

            $product[$key] = $value['NAME'] .' x '. $value['QTY'];
        }
        $insert_transaction_detail = $this->master->create($this->table_2, $data['dua']);

        // End Email Notifikasi
        $text='<h3 class="mb-3">Thank you for shopping with STUDIO POP</h3>';
        $text.="<p><strong>Hi,&nbsp;".$request->name."</strong></p>";
        $text.="<p class='mb-3'><strong>Thank you for your order! Your items will be delivered after the payment has been made. Please make your payment within 24 hours to avoid order cancellation and send the payment receipt to shopping@studiopop.id</strong></p>";
        $text.="<div class='box-text'>";
        $text.="<p><strong>Order No : ".$tr_id."</strong></p>";
                $shipping=$request->shipping;
                $subtotal=0;
                if(!empty($request->product)){
                foreach ($request->product as $item){
                    $sub=$item['QTY']*$item['PRICE'];
                    $subtotal+=$sub;
                }}
                $total=$shipping+$subtotal;
        $text.="<p><strong>SubTotal : Rp. ".number_format($subtotal)."</strong></p>";
        $text.="<p><strong>Shipping : Rp. ".number_format($shipping)."</strong></p>";
        $text.="<p class='mb-3'><strong>Total &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Rp. ".number_format($total)."</strong></p>";
        $text.="<p><i><strong>Please settle the payment by transferring it to the following bank account with your order number as the reference.<br><br>

        BCA 7310160371 a/n Daiva Prayudi </strong></i></p>";
        $text.='<hr class="dash"></br>';

        $text.='<center><p class="text-center">STUDIO POP Terms</p>';
        $text.='<p class="text-center">All sales are final. No refunds. No exchanges.</p><br>';

        $text.='<p class="text-center">Shipping</p>';
        $text.='<p class="text-center">Kindly make sure that the shipping address is correct and somebody is home to receive the package from 9 am to 6 pm on the estimated delivery date.</p><br>';

        $text.='<p class="text-center">Please transfer to</p>';

        $text.='<p class="text-center">BCA 7310160371 a/n Daiva Prayudi</p><br>';

        $text.='<p class="text-center">Please complete your payment within 24 hours. The order will automatically be canceled if there’s no payment within 24 hours.</p>';
        $text.='<p class="text-center">Please send a confirmation after you make the payment along with the prove of payment</p><br>';

        $text.='<p class="text-center">Once again, thank you for shopping with STUDIO POP!</p><br>';

        $text.='<p class="text-center"><strong>STUDIO POP</strong></p>';
        $text.='<p class="text-center">www.studiopop.id</p></div></center>';

        $msc['NAME']=$request->name;
        $msc['MAILTO']=$request->email;
        $msc['SUBJECT']='Studio Pop Notificaton';
        $msc['BODY']=$text;
        
        
        $status = $this->php_mail->sendMail($msc);

        return $this->response_data(
            'Result Data Checkout Success!', 
            [
                'self'          => url($request->fullURL()),
            ], 
            [
                "NAME"          => $request->name,
                "NO_TR"         => $tr_id,
                "PRODUCT"       => $product,
                "COUNT_PRICE"   => $request->count_payment
            ]
        );
    }
}
