<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Master extends Model
{
    // Results Array Default
    public function result_all($table, $select) {
        $results = DB::table($table)->select($select)->get();

        return $results;
    }

    // Results Array With Filter Where
    public function results_filtering($table, $conditional, $select, $name_orderBy, $orderBy) {
        $results = DB::table($table)->where($conditional)->select($select)->orderBy($name_orderBy, $orderBy)->get();

        return $results;
    }

    // Result Object With Filter Where
    public function result_filtering($table, $conditional, $select) {
        $result = DB::table($table)->where($conditional)->select($select)->first();

        return $result;
    }
    
    // Create Data
    public function create($table, $data) {
        $insert = DB::table($table)->insert($data);

        return $insert;
    }

    // Get id after Create Data
    public function getID_create($table, $data) {
        $insert = DB::table($table)->insertGetId($data);

        return $insert;
    }

    // Updates Data
    public function updates($table, $conditional, $data) {
        $update = DB::table($table)->where($conditional)->update($data);

        return $update;
    }


    // ======================================== Model Content ========================================= //
    public function result_content_journal_playlist($table, $select, $limit) {
        $content = DB::table($table)->where('STATUS', '=', 1)
                    ->where(function($data) {
                        $data->where('CATEGORY', 1)->orWhere('CATEGORY', 2);
                    })->select($select)->take($limit)->orderBy('ID', 'desc')->get();
        
        return $content;
    }

    public function result_content($table, $conditional, $select, $page, $perpage, $order=null) {
        $content = DB::table($table)
                    ->where($conditional)
                    ->select($select)->forPage($page, $perpage);

                    if ($order != null) {
                        $content = $content->orderBy($order, 'desc');
                    }
        
        return $content->get();
    }

    public function count_result_content($table, $conditional) {
        $content = DB::table($table)
                    ->where($conditional)
                    ->count();
        
        return $content;
    }

    public function result_find_content($table, $conditional, $select) {
        $content = DB::table($table)
                    ->where($conditional)
                    ->select($select)->first();
        
        return $content;
    }

    // ========================================== Model Banners ========================================= //
    public function result_banner($table, $conditional, $select, $limit) {
        $banner = DB::table($table)
                    ->where($conditional)
                    ->select($select)->take($limit)->orderBy('ID', 'desc')->get();
        
        return $banner;
    }

    // ======================================== Module Config Apps ======================================= //
    public function result_config($table, $conditional, $select) {
        $apps = DB::table($table)
                ->where($conditional)
                ->select($select)->orderBy('ORDNUM', 'ASC')->get();

        return $apps;
    }

    // =================================== Module Product ============================================== //
    public function result_product($table, $conditional, $select, $page, $perpage) {
        $product = DB::table($table)
                    ->where($conditional)
                    ->select($select)->forPage($page, $perpage)->orderBy('ID', 'desc')->get();
        
        return $product;
    }

    public function count_result_product($table, $conditional) {
        $product = DB::table($table)
                    ->where($conditional)
                    ->count();
        
        return $product;
    }

    public function result_find_product($table, $conditional, $select) {
        $product = DB::table($table)
                    ->where($conditional)
                    ->select($select)->first();

        if ($product) {
            // Find Data Add Image Product
            $add_image = DB::table('tbl_addimage')->where('PRODUCT_ID', $product->ID)->get();
        } else {
            
        }
        
        return $product;
    }

    public function result_find_category($table, $conditional, $select) {
        $product = DB::table($table)
                    ->where($conditional)
                    ->select($select)->first();
                    
        return $product;
    }

    // ================================================ Models Add_image Product ============================================= //
    public function result_add_image($table, $conditional, $select) {
        $add_image = DB::table($table)
                ->where($conditional)
                ->select($select)->orderBy('ID', 'ASC')->get();

        return $add_image;
    }   

    // ====================================== Models Size Stock Product ======================================================= //
    public function result_size($table, $conditional, $select) {
        $size = DB::table($table)
                ->where($conditional)
                ->select($select)->orderBy('ID', 'ASC')->get();

        return $size;
    }
}
