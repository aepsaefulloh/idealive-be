<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;

class Auth extends Model
{
    public function login($table, $username, $password) {
        // Find Akun
        $find_account = DB::table($table)->where('USERNAME', $username)->first();

        if ($find_account) {
            // Verifikasi Password Hash
            $verifikasi_password = Hash::check($password, $find_account->PASSWD);

            // Find Akun Grup
            $role = DB::table('tbl_group')->where('ID', $find_account->ID_GROUP)->where('STATUS', 1)->first();

            if ($role && $find_account->USERNAME == $username && $verifikasi_password == true && $role->ID == $find_account->ID_GROUP) {
                // Insert Token table User
                $cek_token = DB::table($table)->where('ID', $find_account->ID)->first();
                
                if ($cek_token && $cek_token->TOKEN == '') {
                    // Generate New Token
                    $token = base64_encode(Str::random(50));
                    DB::table($table)->where('ID', $find_account->ID)->update([
                        "TOKEN" => $token
                    ]);
                } 

                $get_token = DB::table($table)->where('ID', $find_account->ID)->first();

                return [
                    "status"    => true,
                    "data"      => [
                        "ID"        => $find_account->ID,
                        "USERNAME"  => $find_account->USERNAME,
                        "EMAIL"     => $find_account->EMAIL,
                        "PHONE"     => $find_account->PHONE,
                        "FULLNAME"  => $find_account->FULLNAME,
                        "ROLE"      => $find_account->ID_GROUP,
                        "JOB"       => $find_account->JOB,
                        "TOKEN"     => $get_token->TOKEN
                    ]
                ];
            } else {
                return [
                    "status"    => false
                ];
            }
        } else {
            return [
                "status"    => false
            ];   
        }
    }
}
