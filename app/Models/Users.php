<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use DB;

class Users extends Model
{
    public function __construct() {
        $this->table = 'tbl_user';
    }

    public function datatables() {
        $account = DB::table($this->table)->select('ID', 'USERNAME', 'EMAIL', 'ID_GROUP', 'LASTLOGIN', 'STATUS')->get();

        if (count($account) > 0) {
            foreach ($account as $key => $act) {
                // Get Role Account 
                $role = DB::table('tbl_group')->where('ID', $act->ID_GROUP)->first();

                // Result Account
                $result[$key] = [
                    "ID"            => $act->ID,
                    "USERNAME"      => $act->USERNAME,
                    "EMAIL"         => $act->EMAIL,
                    "GROUP"         => [
                        "ID"    => $role->ID,
                        "NAME"  => $role->GROUP_NAME
                    ],
                    "LASTLOGIN"     => $act->LASTLOGIN,
                    "STATUS"        => $act->STATUS
                ];
            }
        } else {
            $result = [];
        }

        return $result;
    }
}
