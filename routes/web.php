<?php

$router->get('/', function () {
    return response()->json("Not Allowed Access!", 404);
});

// Authentication
$router->post('/login', 'Auth\AuthController@login');
$router->get('/logout/{id}', ['middleware' => 'auth', 'uses' => 'Auth\AuthController@logout']);
$router->get('/users-logs/{id}', ['middleware' => 'auth', 'uses' => 'Auth\AuthController@logs']);

// Routing Superadmin
$router->group(['prefix' => 'api-admin'], function () use ($router) {
    // Module Account User
    $router->get('/account-user', 'Superadmin\Account\AccountController@result_all');
    $router->get('/account/{id}', 'Superadmin\Account\AccountController@result_find');
    $router->post('/account-store', 'Superadmin\Account\AccountController@store');

    // Module Role Account User
    $router->get('/account-role', 'Superadmin\Account\RoleController@result_all');
    $router->get('/account-role-list', 'Superadmin\Account\RoleController@result_list');
    $router->get('/account-role/{id}', 'Superadmin\Account\RoleController@result_find');
    $router->post('/account-role-store', 'Superadmin\Account\RoleController@store');

    // Module Banner
    $router->get('/content-banner', 'Superadmin\Content\BannerController@results_filtering');
    $router->get('/content-banner/{id}', 'Superadmin\Content\BannerController@result_find');
    $router->post('/content-banner-store', 'Superadmin\Content\BannerController@store');

    // Module Category
    $router->get('/category', 'Superadmin\Category\CategoryController@results_filtering');
    $router->get('/category-list', 'Superadmin\Category\CategoryController@results_list');
    $router->get('/category/{id}', 'Superadmin\Category\CategoryController@result_find');
    $router->post('/category-store', 'Superadmin\Category\CategoryController@store');

    // Module Content
    $router->get('/content', 'Superadmin\Content\ContentController@results_filtering');
    $router->get('/content/{id}', 'Superadmin\Content\ContentController@result_find');
    $router->post('/content-store', 'Superadmin\Content\ContentController@store');
    $router->post('/content-store-image-content', 'Superadmin\Content\ContentController@upload_image_content');
    
    // Module Project
    $router->get('/project', 'Superadmin\Project\ProjectController@results_filtering');
    $router->get('/project/{id}', 'Superadmin\Project\ProjectController@result_find');
    $router->post('/project-store', 'Superadmin\Project\ProjectController@store');
    $router->post('/project-store-image-project', 'Superadmin\Project\ProjectController@upload_image_project');


    // Module Product
    $router->get('/product', 'Superadmin\Product\ProductController@results_filtering');
    $router->get('/product-list', 'Superadmin\Product\ProductController@results_list');
    $router->get('/product/{id}', 'Superadmin\Product\ProductController@result_find');
    $router->post('/product-store', 'Superadmin\Product\ProductController@store');
    $router->get('/product-remove-image', 'Superadmin\Product\ProductController@remove_image');
    $router->post('/product-status', 'Superadmin\Product\ProductController@status_product');

    // Module Stock Product
    $router->get('/stock', 'Superadmin\Product\StockController@results_filtering');
    $router->get('/stock-balance', 'Superadmin\Product\StockController@stock_balance');
    $router->get('/stock/{id}', 'Superadmin\Product\StockController@result_find');
    $router->post('/stock-store', 'Superadmin\Product\StockController@store');

    // Module Transaction
    $router->get('/payment-transaction', 'Superadmin\Payment\TransactionController@results_filtering');
    $router->get('/payment-transaction/{id}', 'Superadmin\Payment\TransactionController@result_find');
    $router->post('/payment-transaction-store', 'Superadmin\Payment\TransactionController@store');

    // Module Setting Apps
    $router->get('/setting-apps', 'Superadmin\Setting\SettingController@results_filtering');
    $router->post('/setting-apps-store', 'Superadmin\Setting\SettingController@store');

    // Module Token Apps
    $router->get('/token-apps', 'Superadmin\Token\TokenController@results_filtering');
    $router->get('/token-apps/{id}', 'Superadmin\Token\TokenController@result_find');
    $router->post('/token-apps-store', 'Superadmin\Token\TokenController@store');
});

$router->group(['prefix' => 'api'], function () use ($router) {
    // Module Content
    $router->get('/content/journal-playlist', 'Api\Content\ContentController@result_all_jurnal_playlist');
    $router->get('/content', 'Api\Content\ContentController@result_all');
    $router->get('/content/{id}', 'Api\Content\ContentController@result_find');

    // Module Project
    $router->get('/project/journal-playlist', 'Api\Project\ProjectController@result_all_jurnal_playlist');
    $router->get('/project', 'Api\Project\ProjectController@result_all');
    $router->get('/project/{id}', 'Api\Project\ProjectController@result_find');
    $router->get('/project/category/{id}', 'Api\Project\ProjectController@result_category');
    $router->get('/category', 'Api\Project\CategoryController@result_all');
    $router->get('/category/{name}', 'Api\Project\CategoryController@result_find');

    // Module Banners
    $router->get('/content-banner/about', 'Api\Content\BannerController@result_about');
    
    // Module Config Apps
    $router->get('/setting-apps', 'Api\SettingController@result_all');

    // Module Product
    $router->get('/product', 'Api\Product\ProductController@result_all');
    $router->get('/product/{id}', 'Api\Product\ProductController@result_find');
    $router->get('/product/category/{id}', 'Api\Product\ProductController@result_category');
    $router->get('/category', 'Api\Product\CategoryController@result_all');
    $router->get('/category/{name}', 'Api\Product\CategoryController@result_find');

    // Module Payment 
    $router->post('/checkout-store', 'Api\Payment\PaymentController@checkout_store'); 
    
    // Public API
    $router->get('/province', 'ToolsController@get_province');
});

$router->get('/email-test', 'ToolsController@send_email');